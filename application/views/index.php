<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/materialize.css" media="screen,projection">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/materialize.min.css" media="screen,projection">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style.css" media="screen,projection">
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/1.9.32/css/materialdesignicons.min.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <nav class="indigo darken-4 z-depth-4">
    <div class="container">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo">Rafi</a>
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a  href="#">Home</a></li>
                <li><a  href="#">About Me</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="#">Home</a></li>
                <li><a href="#">About Me</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
        </div>
        </div>
    </nav>


    <div class="col s12 home z-depth-4">
        <br>
            <br>
            <h1 class="hom">Selamat Datang</h1>
            <h6 class="hom">Scroll kebawah kalau mau lihat lihat </h6>
            
        <a class="btn waves-effect indigo darken-5-text darken-text-2 homi">Kenali Saya</a>
    </div>


    <div class="container-fluid">
        <div class="row">
            <div class="col s12 as z-depth-4">
                 <div class="grid-example col s12 m6">
                    <h2 class="a">Muhammad Rafi Shalahudin</h2>
                    <h5 class="a">(Rafi)</h5>
                </div>
                 <div class="grid-example col s12 m6">
                    <img class="responsive-img" src="<?php echo base_url(); ?>images/rafi.png">
                </div>
            </div>
            </div>
            </div>
<div class="container">
<div class="row">
<div class="col s12 info">
            <div class="card">
    <div class="card-tabs">
      <ul class="tabs tabs-fixed-width">
        <li class="tab"><a href="#test4" >Alamat</a></li>
        <li class="tab"><a class="active" href="#test5">Tempat Tanggal Lahir</a></li>
        <li class="tab"><a href="#test6">Agama</a></li>
      </ul>
    </div>
    <div class="card-content white lighten-4 z-depth-1">
      <div id="test4">Jl.Ters.Logam Buanasari 1 No.3 RT07 RW04 Kel Kujang sari Kec Bandung kidul Kota Bandung</div>
      <div id="test5">Bandung , 1 Februari 2000</div>
      <div id="test6">Islam</div>
    </div>
  </div>
  </div></div></div>

             <div class="container">
        <div class="row">
            <div class="col s12 riwayat">
                 <div class="grid-example col s12 aabb">
                   <h2 class="ab">Riwayat Pendidikan</h2>
                    <button data-target="modal1" class="btn ss">Klik untuk mengetahui</button>
                </div>
            </div>
            </div>
            </div>


  
    <div class="row">
        
    
            <div class="col s12 lak">
                <h4 class="b">Apa yang saya lakukan?</h4>
                <h6 style="text-align: center;">Kegiatan sehari hari yang saya lakukan di dunia IT</h6>
		<div class="col s12 m4">
		 <div class="card ">
            <div class="card-image">
            <img src="<?php echo base_url(); ?>images/web.jpg">
              <span class="card-title"><b>Programmer</b></span>
            </div>
            <div class="card-content">
              <p>Programmer adalah seseorang yang mampu menyelesaikan masalah dengan menggunakan bahasa pemrograman. Mereka handal dalam menulis kode.</p>
            </div>
             <div class="card-action">
            <label>Penguasaan</label>
              <div class="progress">
      <div class="determinate" style="width: 80%"></div>
  </div>
            </div>
          </div>
          </div>
          <div class="col s12 m4">
         <div class="card">
            <div class="card-image">
              <img src="<?php echo base_url(); ?>images/pc.jpg">
              <span class="card-title"><b>Desainer Grafis</b></span>
            </div>
            <div class="card-content">
              <p>Desainer grafis (bahasa Inggris: Graphic Designer) adalah profesi yang menciptakan ilustrasi, tipografi, fotografi, atau grafis motion. </p>
            </div>
             <div class="card-action">
            <label>Penguasaan</label>
              <div class="progress">
      <div class="determinate" style="width: 50%"></div>
  </div>
            </div>
          </div>
          </div>
          <div class="col s12 m4">
         <div class="card">
            <div class="card-image">
              <img src="<?php echo base_url(); ?>images/des.jpg">
              <span class="card-title"><b> PC Build</b></span>
            </div>
            <div class="card-content">
              <p>Suka dengan hal hal berbau harware,terutama komponen pc terbaru. Merupakan seorang hardware freak yang suka membongkar dan overclock PC.</p>
            </div>
            <div class="card-action">
            <label>Penguasaan</label>
              <div class="progress">
      <div class="determinate" style="width: 70%"></div>
  </div>
            </div>
          </div>
          </div>
          </div>
        </div>  
      </div>

    </div>
    </div>

    <footer class="page-footer indigo darken-3">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <form class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <textarea class="materialize-textarea"></textarea>
          <label style="color: white">Saran terhadap web ini</label>
          <a class="waves-effect btn indigo darken-2" onclick="Materialize.toast('Terimakasih atas masukannya', 1000)">Kirim</a>
        </div>

      </div>
    </form>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Sosial Media</h5>
                    <ul>
                        <li><i class="mdi mdi-facebook-box"><a class="grey-text text-lighten-3" href="#!"> Facebook </a></i></li>
                        <li><i class="mdi mdi-twitter-box"><a class="grey-text text-lighten-3" href="#!"> Twitter</a></i></li>
                        <li><i class="mdi mdi-message-processing"><a class="grey-text text-lighten-3" href="#!"> Line</a></i></li>
                        <li><i class="mdi mdi-instagram"><a class="grey-text text-lighten-3" href="#!"> Instagram</a></i></li>
                        <li><i class="mdi mdi-mail-ru"><a class="grey-text text-lighten-3" href="#!"> rafisalahudin248@gmail.com</a></i></li>
                        <li><i class="mdi mdi-phone"><a class="grey-text text-lighten-3" href="#!"> 0838-2252-3876</a></i></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2017 Muhammad Rafi Shalahudin
                <a class="grey-text text-lighten-4 right" href="#!">SMK Negeri 4 Bandung</a>
            </div>
        </div>
    </footer>

     <!-- Modal Trigger -->

  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Riwayat Pendidikan</h4>
      
      <div class="col s12 m7 card">
    <div class="card horizontal">
      <div class="card-image">
        <img src="<?php echo base_url(); ?>images/sd.png" style="width: 60px; height: 60px; margin-left: 5px;margin-top: 5px;">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Bersekolah di SDN Jakapurwa 2 (2006 - 2012)</p>
        </div>
      </div>
    </div>
  </div>

    <div class="col s12 m7 card">
    <div class="card horizontal">
      <div class="card-image">
        <img src="<?php echo base_url(); ?>images/31.jpg" style="width: 60px; height: 60px; margin-left: 5px;margin-top: 5px;">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Melanjutkan sekolah ke SMP Negeri 31 Bandung (2012 - 2015)</p>
        </div>
      </div>
    </div>
  </div>

    <div class="col s12 m7 card">
    <div class="card horizontal">
      <div class="card-image">
        <img src="<?php echo base_url(); ?>images/4.png" style="width: 60px; height: 60px; margin-left: 5px;margin-top: 5px;">
      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Dari SMPN 31 Bandung dilanjut lagi ke SMK Negeri 4 Bandung (2015 - 2018)</p>
        </div>
      </div>
    </div>
  </div>

    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Tutup</a>
    </div>
  </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/materialize.min.js"></script>
    <script type = 'text/javascript' src = "<?php echo base_url(); ?>js/carousel.js"></script>
</body>

</html>
